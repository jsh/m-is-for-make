info:                                        
	sleep 60; echo before info           # this never even happens!
	$(info an info string)               # no line number
	# $(info within a comment)           # eval'd within comments!
	$(warning a warning string)          # warning with line number
	$(error an error string)             # fail immediately w. lineno
	echo after error call                # fail even if make -i -k -n

hello: hello.c hello.h
  ifdef DEBUG                                # make has ifdefs
	echo some debugging info
  endif                                      # indent with space, not tab
	cc $< -o $@ 
